package com.craterzone.registration;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.widget.TextView;

public class MainActivity extends FragmentActivity {
	TextView textView1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

	      FragmentManager fragmentManager = getFragmentManager();
	      FragmentTransaction fragmentTransaction =  fragmentManager.beginTransaction();
	      fragmentTransaction.add(R.id.fragment_container, new FirstFrag() , "firstFragment").commit();
	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		FirstFrag fragment = (FirstFrag)getFragmentManager().findFragmentByTag("firstFragment");
		if(fragment.isVisible()){
			finish();
			return true;
		}else{
			if(getFragmentManager().getBackStackEntryCount() != 0){
				getFragmentManager().popBackStack();
			}
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
}
