package com.craterzone.registration;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

public class FirstFrag extends Fragment implements OnClickListener {

	private View mRootView;
	private EditText mcountry_code;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_first, container, false);
		mcountry_code = (EditText) mRootView.findViewById(R.id.editText1);
		Bundle bundle = getArguments();
		if (bundle != null) {

			mcountry_code.setText(bundle.getString("countryname"));
			Toast.makeText(getActivity(), bundle.getString("countryname"),Toast.LENGTH_SHORT).show();
		}
		return mRootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		mRootView.findViewById(R.id.btn).setOnClickListener(this);
		mRootView.findViewById(R.id.mobile_verify).setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == (R.id.mobile_verify)) {
			String country = ((EditText) mRootView.findViewById(R.id.editText1)).getText().toString().trim();
			String mobileNumber = ((EditText) mRootView.findViewById(R.id.emobile)).getText().toString().trim();
						
			if ((!country.equalsIgnoreCase(""))&&mobileNumber.length()<=16&&mobileNumber.length()>=7) {
					Toast.makeText(getActivity(),country + " " +mobileNumber, Toast.LENGTH_SHORT).show();
					SecondFrag fragment = new SecondFrag();
					Bundle bundle = new Bundle();
					bundle.putString("mobile",mobileNumber );
					bundle.putString("country", country);
					fragment.setArguments(bundle);
					FragmentManager fm = getFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					ft.replace(R.id.fragment_container, fragment,"secondfragment").addToBackStack("firstFragment").commit();
			} else {
				Toast.makeText(getActivity(),"Enter valid mobile number ",Toast.LENGTH_SHORT).show();
			}
		} else if (view.getId() == (R.id.btn)) {
			CountryFrag fragment = new CountryFrag();
			Bundle bundle = new Bundle();
			bundle.putString("name", "Name");
			fragment.setArguments(bundle);
			FragmentManager fm = getFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			fm.popBackStack();
			ft.replace(R.id.fragment_container, fragment, "countryfragment").addToBackStack("firstFragment").commit();
		} else
			Toast.makeText(getActivity(), "Try again to select country ",
					Toast.LENGTH_SHORT).show();

	}

}
