package com.craterzone.registration;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class CountryFrag extends Fragment {

	View mRootView,view;
	ListView listview;
	/** Array of countries used to display in CountryListFragment */
	String list[] = new String[] { "India", "Pakistan", "Sri Lanka", "China",
			"Bangladesh", "Nepal", "Afghanistan", "North Korea", "South Korea",
			"Japan", "Bhutan" };
	

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragcountry, container, false);
		
			listview=(ListView) mRootView.findViewById(R.id.countrylist);
		ArrayAdapter<String> adaptor = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1, list);
		 listview.setAdapter(adaptor);
		 listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				String str=listview.getItemAtPosition(arg2).toString();
				FirstFrag fragment = new FirstFrag();
				Bundle bundle = new Bundle();
				bundle.putString("countryname", str);
				fragment.setArguments(bundle);
				FragmentManager fm = getFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				fm.popBackStack();
				ft.replace(R.id.fragment_container, fragment, "firstFragment").commit();			
			}
			 
		 });

		return mRootView;
	}
	
	public void onResume() {
		super.onResume();
	}
}
