package com.craterzone.registration;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class SecondFrag extends Fragment implements OnClickListener{

	View mRootView;
	private String mmobile, mcountry,mfirstname,mlastname,memail,mpass,mrepass;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater
				.inflate(R.layout.fragment_second, container, false);
		Bundle bundle = getArguments();
		if (bundle != null) {
			mmobile = bundle.getString("mobile");
			mcountry = bundle.getString("country");

			Toast.makeText(getActivity(), bundle.getString("countryname"),
					Toast.LENGTH_SHORT).show();
		}
		return mRootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		mRootView.findViewById(R.id.reg).setOnClickListener(this);
	}

	public void onClick(View v) {
		
		ThirdFrag fragment = new ThirdFrag();
		Bundle bundle = new Bundle();
		RadioGroup gendergroup = (RadioGroup) mRootView.findViewById(R.id.gender);
		RadioButton gendersex = ((RadioButton) mRootView.findViewById(gendergroup.getCheckedRadioButtonId()));
		mfirstname=((EditText) (mRootView.findViewById(R.id.firstname))).getText().toString();
		mlastname=((EditText) (mRootView.findViewById(R.id.lastname))).getText().toString();
		memail=((EditText) (mRootView.findViewById(R.id.email))).getText().toString();
		mpass= ((EditText) (mRootView.findViewById(R.id.pass))).getText().toString();
		mrepass= ((EditText) (mRootView.findViewById(R.id.repass))).getText().toString();
		if(!mfirstname.equalsIgnoreCase("")&&!mlastname.equalsIgnoreCase("")&& !memail.equalsIgnoreCase("")){
		if(mpass.equalsIgnoreCase(mrepass))
		{
			bundle.putString("mobile", mmobile);
			bundle.putString("country", mcountry);
			bundle.putString("firstname", mfirstname);
			bundle.putString("lastname", mlastname);
			bundle.putString("email", memail);
			bundle.putString("sex", gendersex.getText().toString());
			fragment.setArguments(bundle);
			FragmentManager fm = getFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			fm.popBackStack();
			ft.replace(R.id.fragment_container, fragment, "thirdfragment").addToBackStack("secondfragment").commit();
		}
		else
			Toast.makeText(getActivity(), "Password are not matched ",Toast.LENGTH_SHORT).show();
	}
		else
			Toast.makeText(getActivity(), "All blocks are mandatory ",Toast.LENGTH_SHORT).show();
	
	}
	
	
}
