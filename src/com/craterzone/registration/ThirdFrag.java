package com.craterzone.registration;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ThirdFrag extends Fragment {
	private String mmobile,mcountry,mfirstname,mlastname,memail,msex;
	private View mRootView;
	private TextView mmobiletv,mcountrytv,mfirstnametv,mlastnametv,memailtv,msextv;
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
		      
			mRootView = inflater.inflate(R.layout.fragment_third, container, false);
			     Bundle bundle = getArguments();
			     if (bundle != null) {
			    	 mmobile=bundle.getString("mobile");
			    	 mcountry=bundle.getString("country");
			    	 mfirstname=bundle.getString("firstname");
			    	 mlastname=bundle.getString("lastname");
			    	 memail=bundle.getString("email");
			    	 msex=bundle.getString("sex");
			     mmobiletv=(TextView) mRootView.findViewById(R.id.mobilrtv);
			     mmobiletv.setText(mmobile);
			     mcountrytv=(TextView) mRootView.findViewById(R.id.countrytv);
			     mcountrytv.setText(mcountry);
			     mfirstnametv=(TextView) mRootView.findViewById(R.id.firstnametv);
			     mfirstnametv.setText(mfirstname);
			     mlastnametv=(TextView) mRootView.findViewById(R.id.lastnametv);
			     mlastnametv.setText(mlastname);
			     memailtv=(TextView) mRootView.findViewById(R.id.emailtv);
			     memailtv.setText(memail);
			     msextv=(TextView) mRootView.findViewById(R.id.gendertv);
			     msextv.setText(msex);
			     
			  }  
			     
		return mRootView;
		   }
	@Override
	public void onResume() {
		super.onResume();
	}

	
}
